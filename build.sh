#!/bin/sh

set -e
set -u

MAKE=${MAKE:-make}

do_clone_build_install() {
	uri="$1"
	name="$2"
	if [ ! -d $name ]; then
		git clone $uri $name
	fi
	cd $name
	$MAKE -j$(nproc)
	sudo $MAKE install
	cd -
}

do_icestorm() {
	do_clone_build_install https://github.com/cliffordwolf/icestorm.git icestorm
}

do_arachne() {
	do_clone_build_install https://github.com/cseed/arachne-pnr.git arachne-pnr
}

do_yosys() {
	do_clone_build_install https://github.com/cliffordwolf/yosys.git yosys
}

main() {
	mkdir -p tools; cd tools
	do_icestorm
	do_arachne
	do_yosys
	cd ..
}

main $@
