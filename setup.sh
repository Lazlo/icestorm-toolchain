#!/bin/sh

set -e
set -u

deps=""
deps="$deps build-essential clang bison flex libreadline-dev"
deps="$deps gawk tcl-dev libffi-dev git mercurial graphviz"
deps="$deps xdot pkg-config python python3 libftdi-dev"
apt-get -q install -y $deps
