# IceStorm Toolchain

Automate building the IceStorm, Arachne-pnr and Yosys.

## Quick Start

Build all tools in one step:

```
sudo ./setup.sh
./build.sh
```

The resulting tools will be installed in ```/usr/local/bin```.

## To Do

 * Create Debian packages
 * Use ccache
